package dao

import (
    "database/sql"
    "fmt"

    "github.com/go-gorp/gorp"
    _ "github.com/go-sql-driver/mysql"
)

type Mobile struct {
    ModelName       string  `json:"model_name,omitempty"`
    PhoneType       string  `json:"phone_type,omitempty"`
    Popularity      string  `json:"popularity,omitempty"`
    Features        string  `json:"features,omitempty"`
    Rating          string  `json:"rating,omitempty"`
    DataSpeed       string  `json:"dataSpeed,omitempty"`
    Ram             string  `json:"ram,omitempty"`
    ProductUrl      string  `json:"productUrl,omitempty"`
    InStock         string  `json:"instock,omitempty"`
    Id              string  `json:"id,omitempty"`
    OperatingSystem string  `json:"operatingsystem,omitempty"`
    Battery         string  `json:"battery,omitempty"`
    ReviewCount     int     `json:"review_count,omitempty"`
    StoreName       string  `json:"store_name,omitempty"`
    LastUpdate      string  `json:"last_update,omitempty"`
    OsVersion       string  `json:"os_version,omitempty"`
    RatingCount     int     `json:"rating_count,omitempty"`
    NetworkType     string  `json:"networktype,omitempty"`
    Price           int     `json:"price,omitempty"`
    SimType         string  `json:"simtype,omitempty"`
    Brand           string  `json:"brand,omitempty"`
    CMCategory      string  `json:"cm_category,omitempty"`
    InternalMemory  string  `json:"internalmemory,omitempty"`
    DiscountType    string  `json:"discount_type,omitempty"`
    ScreenSize      string  `json:"screensize,omitempty"`
    PriceRange      string  `json:"price_range,omitempty"`
    Slug            string  `json:"slug,omitempty"`
    ProductId       string  `json:"product_id,omitempty"`
    ImageUrl        string  `json:"imageUrl,omitempty"`
    Title           string  `json:"title,omitempty"`
    Processor       string  `json:"processor,omitempty"`
    SecondaryCamera float64 `json:"secondarycamera,omitempty"`
    PrimaryCamera   float64 `json:"primarycamera,omitempty"`
    ProductCategory string  `json:"product_category,omitempty"`
    GroupId         string  `json:"group_id,omitempty"`
    SpecId          string  `json:"spec_id,omitempty"`
}

func InitDb() (*gorp.DbMap, error) {
    db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@%s([%s]:%s)/%s", "root", "123", "tcp", "localhost", "3306", "cmpmunafa"))

    if err != nil {
        fmt.Printf("Sql open failed: %v", err)
        return nil, err
    }

    if err := db.Ping(); err != nil {
        return nil, err
    }

    // construct a gorp DbMap
    dbmap := &gorp.DbMap{Db: db, Dialect: gorp.MySQLDialect{}}
    dbmap.AddTableWithName(Mobile{}, "mobiles_mobiles_table").SetKeys(true, "Id")

    // dbmap.Exec(viewQuery)
    return dbmap, nil
}

type MobileDAO struct {
    DbMap *gorp.DbMap
}

func (md *MobileDAO) ByLeastPrice() ([]Mobile, error) {
    //filters are ignored for now
    viewQuery := `SELECT
 t.phone_type PhoneType,
 t.features Features,
 t.rating Rating,
 t.dataSpeed DataSpeed,
 t.store_name StoreName,
 t.imageUrl ImageUrl,
 t.productUrl ProductUrl,
 t.instock Instock,
 t.review_count ReviewCount,
 t.title Title,
 t.last_update LastUpdate,
 t.rating Rating,
 t.rating_count RatingCount,
 t.brand Brand,
 t.cm_category CMCategory,
 t.product_id ProductId,
 t.price Price,
 t.group_id_id GroupId,
 t.spec_id_id SpecId,
 t.battery Battery,
 t.processor Processor,
 t.os_version OsVersion,
 t.model_name ModelName,

 s.os OperatingSystem,
 s.screensize ScreenSize,
 s.secondarycamera SecondaryCamera,
 s.primarycamera PrimaryCamera,
 s.networktype NetworkType,
 s.simtype SimType,
 s.internalmemory InternalMemory,
 s.ram Ram
   FROM mobiles_mobiles_table t, mobiles_mobiles_spec_table s
   WHERE t.spec_id_id=s.spec_id AND t.instock=1 AND  t.price = ( SELECT MIN( price ) FROM mobiles_mobiles_table AS f WHERE f.group_id_id = t.group_id_id )
   GROUP BY t.group_id_id`

    var list []Mobile
    if _, err := md.DbMap.Select(&list, viewQuery); err != nil {
        return nil, err
    }

    return list, nil
}

func (md *MobileDAO) GetSpec(grpId string) (string, error) {
    query := `select specification from mobiles_mobiles_group_id where group_id =?`
    return md.DbMap.SelectStr(query, grpId)
}

type CashBacks struct {
    Id              int    `db:"id"`
    StoreId         string `db:"store_name_id"`
    ProductCategory string `db:"product_category"`
    DiscountType    string `db:"discount_type"`
    PriceRange      string `db:"price_range"`
}

func (md *MobileDAO) CheckCashBack(sname, pcat string) ([]CashBacks, error) {
    var cbs []CashBacks
    if _, err := md.DbMap.Select(&cbs,
        "SELECT * FROM cashbacksettings_cashbacksettings WHERE product_category IN ('all',?) AND store_name_id=?",
        pcat, sname); err != nil {
        return nil, err
    }
    return cbs, nil
}
