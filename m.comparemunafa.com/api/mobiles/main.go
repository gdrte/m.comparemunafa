package main

import (
    "fmt"
    "net/http"
    "os"
    "runtime"

    "time"

    "github.com/DevanandReddy/web"
    "github.com/pmylund/go-cache"
    "github.com/robfig/cron"
    "m.comparemunfa.com/api/mobiles/controllers"
    "m.comparemunfa.com/api/mobiles/dao"
)

var (
    GlobalConfig = struct {
        Cache *cache.Cache
        Dao   *dao.MobileDAO
    }{Cache: cache.New(time.Minute*60, time.Minute*2)}
)

func main() {
    runtime.GOMAXPROCS(runtime.NumCPU())
    dbMap, err := dao.InitDb()
    if err != nil {
        fmt.Printf("Error in opening connection %v\n", err)
        os.Exit(1)
    }

    context := make(map[string]interface{})
    dao := dao.MobileDAO{dbMap}
    context["MDao"] = &dao
    context["Cache"] = GlobalConfig.Cache

    GlobalConfig.Dao = &dao
    startScheduler()
    cacheDataHandler()

    router := web.NewWithContext(controllers.MobileContext{}, context).Middleware(web.LoggerMiddleware)
    router.Get("/mobiles", (*controllers.MobileContext).ListAll)
    router.Get("/mobiles/:groupId/spec", (*controllers.MobileContext).GetSpec)
    fmt.Println("Ready to Go!!")
    http.ListenAndServe("localhost:8000", router)
}

func startScheduler() {
    c := cron.New()
    c.AddFunc("@every 5m", cacheDataHandler)
    c.Start()
}

func cacheDataHandler() {
    if mergeResults, err := GlobalConfig.Dao.ByLeastPrice(); err == nil {
        for i, mobile := range mergeResults {
            if cbs, err := GlobalConfig.Dao.CheckCashBack(mobile.StoreName, "mobiles"); err == nil {
                if len(cbs) == 1 {
                    mergeResults[i].DiscountType = cbs[0].DiscountType
                    mergeResults[i].PriceRange = cbs[0].PriceRange
                } else {
                    for _, cb := range cbs {
                        if cb.ProductCategory == "mobiles" {
                            mergeResults[i].DiscountType = cb.DiscountType
                            mergeResults[i].PriceRange = cb.PriceRange
                        }
                    }
                }
            } else {
                fmt.Printf("%v", err)
            }
        }
        GlobalConfig.Cache.Set("ByLeastPrice", mergeResults, cache.NoExpiration)
        fmt.Println("Cache updated")
    } else {
        fmt.Printf("Error while caching :%v", err)
    }
}
