package controllers

import (
    "encoding/json"
    "fmt"
    "sort"

    "net/url"
    "sync"

    "strconv"
    "strings"

    "github.com/DevanandReddy/web"
    "github.com/pmylund/go-cache"
    "m.comparemunfa.com/api/mobiles/dao"
)

type MobileContext struct {
    MDao  *dao.MobileDAO
    Cache *cache.Cache
}

func (mc *MobileContext) ListAll(rw web.ResponseWriter, req *web.Request) {
    rw.Header().Set("Content-Type", "application/json")
    mergeResults, ok := mc.Cache.Get("ByLeastPrice")
    if ok {
        var fmobiles []dao.Mobile
        if len(req.URL.Query()) > 0 {
            fmobiles = filter(mergeResults.([]dao.Mobile), req.URL.Query())
        } else {
            fmobiles = mergeResults.([]dao.Mobile)[0:100]
        }
        buff, _ := json.Marshal(fmobiles)
        fmt.Fprintf(rw, string(buff))
    } else {
        fmt.Fprintf(rw, "%v", "Cached results not found")
    }
}

func (mc *MobileContext) GetSpec(rw web.ResponseWriter, req *web.Request) {
    rw.Header().Set("Content-Type", "application/json")
    if spec, err := mc.MDao.GetSpec(req.PathParams["groupId"]); err == nil {
        fmt.Fprintf(rw, spec)
    } else {
        fmt.Fprintf(rw, "%v", err)
    }
}

func fanOutMobiles(mobiles []dao.Mobile, start, end int, query url.Values) <-chan int {
    out := make(chan int)
    go func() {
        for i := start; i < end; i++ {
            if queryFilter(&mobiles[i], query) {
                out <- i
            }
        }
        close(out)
    }()
    return out
}

func filter(mobiles []dao.Mobile, query url.Values) []dao.Mobile {
    fmobiles := make([]dao.Mobile, 1, 10)
    mlen := len(mobiles)
    gsize := mlen / 4

    if gsize < 4 {
        for mi := range fanOutMobiles(mobiles, 0, mlen, query) {
            fmobiles = append(fmobiles, mobiles[mi])
        }
    } else {
        mc1 := fanOutMobiles(mobiles, 0, gsize, query)
        mc2 := fanOutMobiles(mobiles, gsize, gsize*2, query)
        mc3 := fanOutMobiles(mobiles, gsize*2, gsize*3, query)
        mc4 := fanOutMobiles(mobiles, gsize*3, mlen, query)

        for mi := range merge(mc1, mc2, mc3, mc4) {
            fmobiles = append(fmobiles, mobiles[mi])
        }
    }

    return fmobiles[1:len(fmobiles)]
}

func queryFilter(mobile *dao.Mobile, query url.Values) bool {
    flag1 := true
    flag2 := true
    for key, val := range query {
        switch key {
        case "brand":
            for _, brand := range val {
                if flag1 = strings.Contains(brand, mobile.Brand); flag1 {
                    break
                }
            }
            break
        case "price":
            // s := strings.Split(val[0], "-")
            low, _ := strconv.Atoi(val[0])
            high, _ := strconv.Atoi(val[1])
            flag2 = (mobile.Price >= low && mobile.Price <= high)
            break
        }
    }

    return flag1 && flag2
}

func merge(cs ...<-chan int) <-chan int {
    var wg sync.WaitGroup
    out := make(chan int)

    // Start an output goroutine for each input channel in cs.  output
    // copies values from c to out until c is closed, then calls wg.Done.
    output := func(c <-chan int) {
        for n := range c {
            out <- n
        }
        wg.Done()
    }
    wg.Add(len(cs))
    for _, c := range cs {
        go output(c)
    }

    // Start a goroutine to close out once all the output goroutines are
    // done.  This must start after the wg.Add call.
    go func() {
        wg.Wait()
        close(out)
    }()
    return out
}

type By func(m1, m2 *dao.Mobile) bool

func (by By) Sort(mobiles []dao.Mobile) {
    ms := &mobileSorter{
        mobiles: mobiles,
        by:      by,
    }
    sort.Sort(ms)
}

type mobileSorter struct {
    mobiles []dao.Mobile
    by      func(m1, m2 *dao.Mobile) bool
}

func (m *mobileSorter) Len() int {
    return len(m.mobiles)
}

func (m *mobileSorter) Swap(i, j int) {
    m.mobiles[i], m.mobiles[j] = m.mobiles[j], m.mobiles[i]
}

func (m *mobileSorter) Less(i, j int) bool {
    return m.by(&m.mobiles[i], &m.mobiles[j])
}
