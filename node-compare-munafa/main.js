var express = require('express')
var Promise = require('bluebird')
var mysql = require('mysql')
var NodeCache = require('node-cache')


Promise.promisifyAll(require("mysql/lib/Pool").prototype)
Promise.promisifyAll(require("mysql/lib/Connection").prototype)


var cache = new NodeCache()
Promise.promisifyAll(cache)

var pool =  mysql.createPool({
    host : "localhost",
    port:3306,
    user : "root",
    password: "123",
    database:'cmpmunafa'
});

var query="SELECT t.phone_type PhoneType, t.features Features, t.rating Rating, t.dataSpeed DataSpeed, t.store_name StoreName, t.imageUrl ImageUrl, t.productUrl ProductUrl, t.instock Instock, t.review_count ReviewCount, t.title Title, t.last_update LastUpdate, t.rating Rating, t.rating_count RatingCount, t.brand Brand, t.cm_category CMCategory, t.product_id ProductId, t.price Price, t.group_id_id GroupId, t.spec_id_id SpecId, t.battery Battery, t.processor Processor, t.os_version OsVersion, t.model_name ModelName, s.os OperatingSystem, s.screensize ScreenSize, s.secondarycamera SecondaryCamera, s.primarycamera PrimaryCamera, s.networktype NetworkType, s.simtype SimType, s.internalmemory InternalMemory, s.ram Ram FROM mobiles_mobiles_table t, mobiles_mobiles_spec_table s WHERE t.spec_id_id=s.spec_id AND t.instock=1 AND  t.price = ( SELECT MIN( price ) FROM mobiles_mobiles_table AS f WHERE f.group_id_id = t.group_id_id ) GROUP BY t.group_id_id";


pool.getConnectionAsync().then(function(conn){
    return conn;
}).then(function(conn){
    conn.queryAsync(query).spread(function(rows,cols){
	console.log(rows.length+' found');
	var promises=rows.map(function(row){
	var squery="SELECT * FROM cashbacksettings_cashbacksettings WHERE product_category IN ('all',?) AND store_name_id=?"
	    return conn.queryAsync(squery,['mobiles',row.StoreName]).spread(function(specRows,cols){
		if (specRows.length==0){
		    return row
		}
		    if (specRows.length==1){
			row.DiscountType=specRows[0].discount_type
			row.PriceRange = specRows[0].price_range
		    }else{
			for (var j=0;j<specRows.length;j++){
			    if (specRows[j].product_category=='mobiles'){
				row.DiscountType=specRows[j].discount_type
				row.PriceRange = specRows[j].price_range
				break;
			    }
			}
		    }
		return row
	    })//end of spread
	})
	Promise.all(promises).then(function(rows){
	    cache.set('mergeResults',rows)
	}).catch(function(err){
	    console.log(err)
	});
    }).catch(function(err){
	    console.log(err)
    })
	}).catch(function(err){
	    console.log(err)
})



var app = express()
app.get('/mobiles', function (req, res) {
	cache.getAsync('mergeResults').then( function(rows){
	    var filtered=rows.filter(function(row){
		return queryFilter(row,req)
	    })
	res.send(filtered)
	}).catch(function(err){
	    console.log(err)
	    res.send(err)
	});
});


queryFilter = function(mobile,req) {
    var flag1 = true
    var flag2 = true
    var brand=req.query['brand']
    if (brand!=null && brand!='undefined'){
	if (!Array.isArray(brand)){
	    flag1 = mobile.Brand.indexOf(brand)>-1
	}else{
	    for(var i=0;i<brand.length;i++){
		flag1 = mobile.Brand.indexOf(brand[i])>-1
		if(flag1){
		    break;
		}
	    }
	}
    }

    if (req.query['price']){
	low = parseInt(req.query['price'][0])
	high = parseInt(req.query['price'][1])

	flag2 = (mobile.Price >= low && mobile.Price <= high)
    }

    return flag1 && flag2
}



var server = app.listen(8001, function () {
  var host = server.address().address;
  var port = server.address().port;
  console.log('Example app listening at http://%s:%s', host, port);
});
